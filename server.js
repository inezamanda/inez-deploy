const express = require('express')
const app = express()
const bookRouter = require('./routers/book')
const port = process.env.PORT || 8000

app.use(express.json())
app.use(express.urlencoded({ extended: true }));

app.use(bookRouter)

app.listen(port, () => {
    console.log('Server ready!')
})